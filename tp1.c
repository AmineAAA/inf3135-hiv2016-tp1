#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define LONGUEUR_MAX 3000
#define NOM_PAYS 50
#define NOM_VILLE 50
#define CODE_PAYS 10
struct Pays {
    char nom[NOM_PAYS];
    char code[CODE_PAYS];
};
struct Ville {
    char nom[NOM_VILLE];
    long population;
    struct Pays pays;
};
static int comparerElts (const void *elt1, const void *elt2){
    struct Ville const *pointeur1 = elt1;
    struct Ville const *pointeur2 = elt2;
    
    return pointeur1->population - pointeur2->population;
    
}
int main(int argc, char *argv[]){
    FILE* countryInfo = NULL;
    FILE* cities = NULL;
    char ligne[LONGUEUR_MAX] = "";
    int k = 0;
    int i = 0;
    char *chaine;
    char *chaine2;
    int j = 0;
    char *chaineTemp;
    char *chaineTemp2;
    char ligne2[LONGUEUR_MAX] = "";
    char *temp = NULL;
 
    if (argc!=2){
        printf("Erreur! Veuillez entrer un et un seul parametre.\n");
        exit(1); 

    }
     long nbAffichage = strtol(argv[1],NULL,10);

if (nbAffichage > 5000 || nbAffichage < 1){
        printf("Erreur! Le parametre entré doit etre entre 1 et 5000 exclusivement.\n");

    }else{
     printf ("Rang    Nom                 Pays                     Population\n");

     printf ("----    ---                 ----                     ----------\n");

    
    static struct Pays tabPays [500];
    countryInfo = fopen("countryInfo.txt", "r");
    cities = fopen("cities15000.txt","r");
    
    if( countryInfo == NULL || cities == NULL ){
        printf("Erreur d'ouverture des fichiers!\n");
        exit(1);
    }
    
    
    while(i <= 51){
        fgets(ligne, LONGUEUR_MAX, countryInfo);
        i++;
    }
    
    do{
        chaine = ligne;
        i = 0;
        chaineTemp  = strsep(&chaine, "\t");
        strcpy(tabPays[j].code, chaineTemp);
        
        while(i!=4){
            chaineTemp  = strsep(&chaine, "\t");
            i++;
        }
        if(chaineTemp !=NULL){
            strcpy(tabPays[j].nom,chaineTemp);
        }
        j++;
        fgets(ligne, LONGUEUR_MAX, countryInfo);
        
    }while(!feof(countryInfo));
    
    static struct Ville tabVilles [30000];
    j = 0;
    do {
        fgets(ligne2, LONGUEUR_MAX, cities);
        k = 0;
        chaine2 = ligne2;
        
        chaineTemp2 = strsep(&chaine2, "\t");
        chaineTemp2 = strsep(&chaine2, "\t");
        i = 2;
        if(chaineTemp2 != NULL){
            strcpy(tabVilles[j].nom, chaineTemp2);
        }
        while(i!=9){
            chaineTemp2 = strsep(&chaine2, "\t");
            i++;
        }
        if(chaineTemp2 != NULL){
            strcpy(tabVilles[j].pays.code,chaineTemp2);
        }
        while(k<9644){
            if(strcmp(tabVilles[j].pays.code,tabPays[k].code) == 0 ){
                tabVilles[j].pays = tabPays[k];
            }
            k++;
        }
        
        while(i!= 15){
            chaineTemp2 = strsep(&chaine2,"\t");
            i++;
        }
        if(chaineTemp2 != NULL){
            tabVilles[j].population = strtol(chaineTemp2,&temp,10);
        }
        j++;
    }while(!feof(cities));
    qsort((void *)tabVilles,23495,sizeof(tabVilles[0]),comparerElts);
    
   int  l = 23494;
   int  m;
   int tailleNom;
     for ( m = 1 ; m <= nbAffichage; m++ ){
      printf("%4d    %-20s%-25s%-16ld\n",m,tabVilles[l].nom,tabVilles[l].pays.nom,tabVilles[l].population);
    l--;
     }
    
    fclose(cities);
    fclose(countryInfo);
}
    return 0;

}
