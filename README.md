# Travail pratique 1

## Description

Ce projet consiste a manipuler deux fichiers contenants plusieurs informations sur des villes partout dans le monde.
Plus précisement, le projet doit pouvoir afficher la liste des villes ayant la plus grande population sur notre planète. Le nombre de villes affiché sera toutefois a la discretion de l'utilisateur.
Ce projet est réalisé dans le cadre du cours Construction et maintenance de logiciels(INF3135), UQAM.
## Auteur

Amine Oufini (OUFA17119004)

## Fonctionnement

Commande:

gcc tp1.c
./tp1 20

Resultat:

Rang   Nom                 Pays                     Population
----    ---                 ----                     ----------
   1    Shanghai            China                    22315474
   2    Buenos Aires        Argentina                13076300
   3    Mumbai              India                    12691836
   4    Mexico City         Mexico                   12294193
   5    Beijing             China                    11716620
   6    Karachi             Pakistan                 11624219
   7    İstanbul           Turkey                   11174257
   8    Tianjin             China                    11090314
   9    Guangzhou           China                    11071424
  10    Delhi               India                    10927986
  11    Manila              Philippines              10444527
  12    Moscow              Russia                   10381222
  13    Shenzhen            China                    10358381
  14    Dhaka               Bangladesh               10356500
  15    Seoul               South Korea              10349312
  16    São Paulo          Brazil                   10021295
  17    Wuhan               China                    9785388
  18    Lagos               Nigeria                  9000000
  19    Jakarta             Indonesia                8540121
  20    Tokyo               Japan                    8336599

Commande:

gcc tp1.c
./tp1 10

Resultat:

Rang    Nom                 Pays                     Population
----    ---                 ----                     ----------
   1    Shanghai            China                    22315474
   2    Buenos Aires        Argentina                13076300
   3    Mumbai              India                    12691836
   4    Mexico City         Mexico                   12294193
   5    Beijing             China                    11716620
   6    Karachi             Pakistan                 11624219
   7    İstanbul           Turkey                   11174257
   8    Tianjin             China                    11090314
   9    Guangzhou           China                    11071424
  10    Delhi               India                    10927986

## Contenu du projet

tp1.c : Contient le code source servant a manipuler les fichiers contenants les données.
README.md : Décrit le projet et son mode de fonctionnment, cite les réferanceset mentionne lauteur.
countryInfo.txt: Fichier servant a donner le nom du pays et son code.
cities15000: Sert a donner le nom des villes et leur population.
Makefile:Complier automatiquement le programme.

## Références
http://stackoverflow.com/questions/3898021/mainint-argc-char-argv
https://www.areaprog.com/c/article-229-argc-et-argv-utilisation-des-parametres-de-la-ligne-de-commande

## Statut
Complet au niveau du code source mais il ya un petit ajustement a faire au niveau de laffichage du tableau.
